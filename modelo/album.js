const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var AlbumSchema = new Schema({
    titulo: String,
    descripcion: String,
    anio: Number,
    imagen: String,
    artista: { type: Schema.ObjectId, ref: 'Artista'}
});

module.exports = mongoose.model('Album', AlbumSchema);