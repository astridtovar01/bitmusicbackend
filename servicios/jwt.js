var jwt = require('jwt-simple');
//var moment = require('moment');
var secret = "soy_secreta";

exports.createToken = function(usuario){
    var payLoad = {
        id: usuario._id,
        nombre: usuario.nombre,
        apellido: usuario.apellido,
        correo: usuario.correo,
        rol: usuario.rol,
        imagen: usuario.imagen
    };

    return jwt.encode(payLoad, secret);
};