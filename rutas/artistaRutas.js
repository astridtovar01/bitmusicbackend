const express = require('express');
const ArtistaControl = require('../control/artistaControl');
const api = express.Router();
const md_auten = require('../middleware/autenticar');

var multipart = require('connect-multiparty');
var md_subirImg = multipart( {uploadDir: './archivos/artistas'} );


api.get('/artista/:id', md_auten.aseguraAuten, ArtistaControl.obtenerArtista);
api.post('/registrar-artista', md_auten.aseguraAuten, ArtistaControl.registrarArtista);
api.get('/listar-artistas/:page?', md_auten.aseguraAuten, ArtistaControl.listarArtistas);
api.put('/editar-artista/:id', md_auten.aseguraAuten, ArtistaControl.actualizarArtista);
api.delete('/eliminar-artista/:id', md_auten.aseguraAuten, ArtistaControl.eliminarArtista);
api.post('/subir-imagen-artista/:id', [md_auten.aseguraAuten, md_subirImg], ArtistaControl.subirImagenArtista);
api.get('/obtener-imagen-artista/:imageFile', ArtistaControl.obtenerArchivo);


module.exports = api;