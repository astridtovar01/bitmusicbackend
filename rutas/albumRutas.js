const express = require('express');
const AlbumControl = require('../control/albumControl');
const api = express.Router();
const md_auten = require('../middleware/autenticar');

var multipart = require('connect-multiparty');
var md_subirImg = multipart( {uploadDir: './archivos/albums'} );


api.get('/obtener-album/:id', md_auten.aseguraAuten, AlbumControl.obtenerAlbum);
api.post('/registrar-album', md_auten.aseguraAuten, AlbumControl.registrarAlbum);
api.get('/listar-albums/:artista?', md_auten.aseguraAuten, AlbumControl.listarAlbums);
api.put('/editar-album/:id', md_auten.aseguraAuten, AlbumControl.actualizarAlbum);
api.delete('/eliminar-album/:id', md_auten.aseguraAuten, AlbumControl.eliminarAlbum);
api.post('/subir-imagen-album/:id', [md_auten.aseguraAuten, md_subirImg], AlbumControl.subirImagenAlbum);
api.get('/obtener-imagen-album/:imageFile', AlbumControl.obtenerArchivo);


module.exports = api;