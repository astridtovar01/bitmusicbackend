const express = require('express');
const UsuarioControl = require('../control/usuarioControl');
const md_auten = require('../middleware/autenticar');

var api = express.Router();

var multipart = require('connect-multiparty');
var md_subirImg = multipart({ uploadDir: './archivos/usuarios' });

// api.get('/usuarioControl', UsuarioControl.pruebas);
api.post('/registro', UsuarioControl.guardarUsuario);
api.post('/login', UsuarioControl.login);
api.put('/actualizar-usuario/:id', md_auten.aseguraAuten, UsuarioControl.editarUsuario);
api.post('/subir-imagen-usuario/:id', [md_auten.aseguraAuten, md_subirImg], UsuarioControl.subirImagen);
api.get('/obtener-imagen-usuario/:imageFile', UsuarioControl.obtenerArchivo);

module.exports = api;