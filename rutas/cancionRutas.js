const express = require('express');
const CancionControl = require('../control/cancionControl');
const api = express.Router();
const md_auten = require('../middleware/autenticar');

var multipart = require('connect-multiparty');
var md_subirImg = multipart( {uploadDir: './archivos/canciones'} );


api.get('/obtener-cancion/:id', md_auten.aseguraAuten, CancionControl.obtenerCancion);
api.post('/registrar-cancion', md_auten.aseguraAuten, CancionControl.registrarCancion);
api.get('/listar-canciones/:album?', md_auten.aseguraAuten, CancionControl.listarCanciones);
api.put('/editar-cancion/:id', md_auten.aseguraAuten, CancionControl.actualizarCancion);
api.delete('/eliminar-cancion/:id', md_auten.aseguraAuten, CancionControl.eliminarCancion);
api.post('/subir-cancion/:id', [md_auten.aseguraAuten, md_subirImg], CancionControl.subirCancion);
api.get('/obtener-archivo-cancion/:cancionFile', CancionControl.obtenerArchivoCancion);


module.exports = api;