const path = require('path');
const fs = require('fs');
const Artista = require('../modelo/artista');
const Album = require('../modelo/album');
const Cancion = require('../modelo/cancion');
const pagination = require('mongoose-pagination');

function obtenerCancion(req, res) {
    // res.status(200).send({message: "Obtener cancion"});
    var cancionId = req.params.id;

    Cancion.findById(cancionId).populate({ path: 'album' }).exec((err, cancion) => {
        if (err) {
            res.status(500).send({ message: "Error en la peticion" });
        } else {
            if (!cancion) {
                res.status(404).send({ message: "La canción no existe" });
            } else {
                res.status(200).send({ cancion });
            }
        }
    });
}

function registrarCancion(req, res) {
    var cancion = new Cancion();
    var parametros = req.body;

    cancion.numero = parametros.numero;
    cancion.tituloCancion = parametros.tituloCancion;
    cancion.duracion = parametros.duracion;
    cancion.urlCancion = 'null';
    cancion.album = parametros.album;

    cancion.save((err, cancionGuardada) => {
        if (err) {
            res.status(500).send({ message: "Error al guardar" });
        } else {
            if (!cancionGuardada) {
                res.status(404).send({ message: "Canción no guardada" });
            } else {
                res.status(200).send({ cancion: cancionGuardada });
            }
        }
    });
}

function listarCanciones(req, res) {
    var albumId = req.params.album;

    if (!albumId) {
        var find = Cancion.find({}).sort('numero');
    } else {
        var find = Cancion.find({ album: albumId }).sort('numero');
    }

    find.populate({
        path: 'album',
        populate: {
            path: 'artista',
            model: 'Artista'
        }
    }).exec((err, canciones) => {
        if (err) {
            res.status(500).send({ message: "Error al mostrar" });
        } else {
            if (!canciones) {
                res.status(404).send({ message: "Canciones no encontradas" });
            } else {
                res.status(200).send({ canciones });
            }
        }
    });
}

function actualizarCancion(req, res) {
    var cancionId = req.params.id;

    Cancion.findByIdAndUpdate(cancionId, req.body, (err, cancionEditada) => {
        if (err) {
            res.status(500).send({ message: 'Error al guardar' });
        } else {
            if (!cancionEditada) {
                res.status(404).send({ message: 'No se pudo actualizar la cancion' });
            } else {
                res.status(200).send({ cancion: cancionEditada });
            }
        }
    });
}

function eliminarCancion(req, res){
    var cancionId = req.params.id;

    Cancion.findByIdAndDelete(cancionId, (err, cancionEliminada) => {
        if (err) {
            res.status(500).send({ message: 'Error al guardar' });
        } else {
            if (!cancionEliminada) {
                res.status(404).send({ message: 'No se pudo eliminar la cancion' });
            } else {
                res.status(200).send({ cancion: cancionEliminada });
            }
        }
    });
}

function subirCancion(req, res){
    var cancionId = req.params.id;
    var nombreArchivo = "No ha subido nada tarado";

    if(req.files){
        var rutaArchivo = req.files.urlCancion.path;
        var partirArchivo = rutaArchivo.split('\\');
        var nombreArchivo = partirArchivo[2];

        var extenCancion = nombreArchivo.split('\.');
        var extenArchivo = extenCancion[1];

        if(extenArchivo == 'mp3' || extenArchivo == 'ogg'){
            Cancion.findByIdAndUpdate(cancionId, {urlCancion: nombreArchivo}, (err, cancionUpdated) => {
                if(err){
                    res.status(500).send({message: "Error al actualizar"});
                }else{
                    if(!cancionUpdated){
                        res.status(404).send({message: "No se pudo actualizar"});
                    }else{
                        res.status(200).send({cancion: cancionUpdated});
                    }
                }
            });
        }else{
            res.status(200).send( {message: 'No es una canción!!'} )
        }

        console.log(rutaArchivo);
    }else{
        res.status(200).send( {message: 'No ha subido ninguna canción'} )
    }
}

function obtenerArchivoCancion(req, res){
    var archivo = req.params.cancionFile;
    var rutaArchivo = './archivos/canciones/'+archivo;

    fs.exists(rutaArchivo, function(exists){
        if(exists){
            res.sendFile(path.resolve(rutaArchivo));
        }else{
            res.status(200).send( {message: 'No existe la mk cancion'} )
        }
    })
}

module.exports = {
    obtenerCancion,
    registrarCancion,
    listarCanciones,
    actualizarCancion,
    eliminarCancion,
    subirCancion,
    obtenerArchivoCancion
}