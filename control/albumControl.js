const path = require('path');
const fs = require('fs');
const Artista = require('../modelo/artista');
const Album = require('../modelo/album');
const Cancion = require('../modelo/cancion');
const pagination = require('mongoose-pagination');

function obtenerAlbum(req, res) {
    //res.status(200).send({message: "Obtener Álbum"});
    var albumId = req.params.id;

    Album.findById(albumId).populate({ path: 'artista' }).exec((err, album) => {
        if (err) {
            res.status(500).send({ message: "Error en la peticion" });
        } else {
            if (!album) {
                res.status(404).send({ message: "El album no existe" });
            } else {
                res.status(200).send({ album });
            }
        }
    });
}

function registrarAlbum(req, res) {
    var album = new Album();

    var parametros = req.body;
    album.titulo = parametros.titulo;
    album.descripcion = parametros.descripcion;
    album.anio = parametros.anio;
    album.imagen = "null";
    album.artista = parametros.artista;

    album.save((err, albumGuardado) => {
        if (err) {
            res.status(500).send({ message: "Error al guardar" });
        } else {
            if (!albumGuardado) {
                res.status(404).send({ message: "Album no guardado" });
            } else {
                res.status(200).send({ album: albumGuardado });
            }
        }
    });
}

function listarAlbums(req, res) {
    var artistaId = req.params.artista;

    if (!artistaId) {
        var find = Album.find({}).sort('titulo');
    } else {
        var find = Album.find({ artista: artistaId }).sort('anio');
    }

    find.populate({ path: 'artista' }).exec((err, albums) => {
        if (err) {
            res.status(500).send({ message: "Error al mostrar" });
        } else {
            if (!albums) {
                res.status(404).send({ message: "Album no encontrado" });
            } else {
                res.status(200).send({ albums });
            }
        }
    });
}

function actualizarAlbum(req, res) {
    var albumId = req.params.id;

    Album.findByIdAndUpdate(albumId, req.body, (err, albumEditado) => {
        if (err) {
            res.status(500).send({ message: 'Error al guardar' });
        } else {
            if (!albumEditado) {
                res.status(404).send({ message: 'No se pudo actualizar el album' });
            } else {
                res.status(200).send({ album: albumEditado });
            }
        }
    });
}

function eliminarAlbum(req, res) {
    var albumId = req.params.id;

    Album.findById(albumId).remove((err, albumEliminado) => {
        if (err) {
            res.status(500).send({ message: 'Error al eliminar el album' })
        } else {
            if (!albumEliminado) {
                res.status(404).send({ message: 'No se pudo eliminar el album' })
            } else {
                Cancion.find({ album: albumEliminado._id }).remove((err, cancionEliminada) => {
                    if (err) {
                        res.status(500).send({ message: 'Error al eliminar las canciones' })
                    } else {
                        if (!cancionEliminada) {
                            res.status(404).send({ message: 'No se pudo eliminar la cancion' })
                        } else {
                            res.status(200).send({ album: albumEliminado });
                        }
                    }
                });
            }
        }
    });
}

function subirImagenAlbum(req, res){
    var albumId = req.params.id;
    var nombreArchivo = "No ha subido nada tarado";

    if(req.files){
        var rutaArchivo = req.files.imagen.path;
        var partirArchivo = rutaArchivo.split('\\');
        var nombreArchivo = partirArchivo[2];

        var extenImg = nombreArchivo.split('\.');
        var extenArchivo = extenImg[1];

        if(extenArchivo == 'png' || extenArchivo == 'jpg'){
            Album.findByIdAndUpdate(albumId, {imagen: nombreArchivo}, (err, albumUpdated) => {
                if(err){
                    res.status(500).send({message: "Error al actualizar"});
                }else{
                    if(!albumUpdated){
                        res.status(404).send({message: "No se pudo actualizar"});
                    }else{
                        res.status(200).send({album: albumUpdated});
                    }
                }
            });
        }else{
            res.status(200).send( {message: 'No es una puta imagen!!'} )
        }

        console.log(rutaArchivo);
    }else{
        res.status(200).send( {message: 'No ha subido ninguna imagen'} )
    }
}

function obtenerArchivo(req, res){
    var archivo = req.params.imageFile;
    var rutaArchivo = './archivos/albums/'+archivo;

    fs.exists(rutaArchivo, function(exists){
        if(exists){
            res.sendFile(path.resolve(rutaArchivo));
        }else{
            res.status(200).send( {message: 'No existe la mk imagen'} )
        }
    })
}


module.exports = {
    obtenerAlbum,
    registrarAlbum,
    listarAlbums,
    actualizarAlbum,
    eliminarAlbum,
    subirImagenAlbum,
    obtenerArchivo
}