const path = require('path');
const fs = require('fs');
const Artista = require('../modelo/artista');
const Album = require('../modelo/album');
const Cancion = require('../modelo/cancion');
const pagination = require('mongoose-pagination');

function obtenerArtista(req, res){
    // res.status(200).send({message: "Metodo obtener artista"});
    var artistaId = req.params.id;

    Artista.findById(artistaId, (err, artista) => {
        if(err){
            res.status(500).send({message: "Error en la peticion"});
        }else{
            if(!artista){
                res.status(404).send({message: "El artista no existe"});
            }else{
                res.status(200).send({artista});
            }
        }
    });
}

function registrarArtista(req, res){
    var artista = new Artista();

    var parametros = req.body;
    artista.nombre = parametros.nombre;
    artista.descripcion = parametros.descripcion;
    artista.imagen = "null";

    artista.save((err, artistaGuardado) => {
        if(err){
            res.status(500).send({message: "Error al guardar"});
        }else{
            if(!artistaGuardado){
                res.status(404).send({message: "Artista no guardado"});
            }else{
                res.status(200).send({artista: artistaGuardado});
            }
        }
    });
};

function listarArtistas(req, res){
    if(req.params.page){
        var page = req.params.page;
    }else{
        var page = 1;
    }
    
    var itemsPerPage = 3;

    Artista.find().sort('nombre').paginate(page, itemsPerPage, function(err, artistas, total){
        if(err){
            res.status(500).send({message: "Error en la petición"});
        }else{
            if(!artistas){
                res.status(200).send({message: "No existen artistas"});
            }else{
                return res.status(200).send({
                    total_items: total,
                    artistas: artistas
                });
            }
        }
    });
}

function actualizarArtista(req, res){
    var artistaId = req.params.id;

    Artista.findByIdAndUpdate(artistaId, req.body, (err, artistaEditado) => {
        if(err){
            res.status(500).send( {message: 'Error al guardar'} );
        }else{
            if(!artistaEditado){
                res.status(404).send( {message: 'No se pudo actualizar el artista'} );
            }else{
                res.status(200).send( {artista: artistaEditado} );
            }
        }
    });
}

function eliminarArtista(req, res){
    var artistaId = req.params.id;

    Artista.findByIdAndRemove(artistaId, (err, artistaEliminado) => {
        if(err){
            res.status(500).send( {message: 'Error al eliminar'} );
        }else{
            if(!artistaEliminado){
                res.status(404).send( {message: 'No se pudo eliminar el artista'} );
            }else{
                Album.find( {artista: artistaEliminado._id} ).remove( (err, albumEliminado) => {
                    if(err){
                        res.status(500).send( {message: 'Error al eliminar el album'} )
                    }else{
                        if(!albumEliminado){
                            res.status(404).send( {message: 'No se pudo eliminar el album'} )
                        }else{
                            Cancion.find( {album: albumEliminado._id} ).remove( (err, cancionEliminada) => {
                                if(err){
                                    res.status(500).send( {message: 'Error al eliminar las canciones'} )
                                }else{
                                    if(!cancionEliminada){
                                        res.status(404).send( {message: 'No se pudo eliminar la cancion'} )
                                    }else{
                                        res.status(200).send( {artista: artistaEliminado} );
                                    }
                                }
                            });
                        }
                    }
                });
            }
        }
    });
}

function subirImagenArtista(req, res){
    var artistaId = req.params.id;
    var nombreArchivo = "No ha subido nada tarado";

    if(req.files){
        var rutaArchivo = req.files.imagen.path;
        var partirArchivo = rutaArchivo.split('\\');
        var nombreArchivo = partirArchivo[2];

        var extenImg = nombreArchivo.split('\.');
        var extenArchivo = extenImg[1];

        if(extenArchivo == 'png' || extenArchivo == 'jpg'){
            Artista.findByIdAndUpdate(artistaId, {imagen: nombreArchivo}, (err, artistaUpdated) => {
                if(err){
                    res.status(500).send({message: "Error al actualizar"});
                }else{
                    if(!artistaUpdated){
                        res.status(404).send({message: "No se pudo actualizar"});
                    }else{
                        res.status(200).send({artista: artistaUpdated});
                    }
                }
            });
        }else{
            res.status(200).send( {message: 'No es una puta imagen!!'} )
        }

        console.log(rutaArchivo);
    }else{
        res.status(200).send( {message: 'No ha subido ninguna imagen'} )
    }
}

function obtenerArchivo(req, res){
    var archivo = req.params.imageFile;
    var rutaArchivo = './archivos/artistas/'+archivo;

    fs.exists(rutaArchivo, function(exists){
        if(exists){
            res.sendFile(path.resolve(rutaArchivo));
        }else{
            res.status(200).send( {message: 'No existe la mk imagen'} )
        }
    })
}

module.exports = {
    obtenerArtista,
    registrarArtista,
    listarArtistas,
    actualizarArtista,
    eliminarArtista,
    subirImagenArtista,
    obtenerArchivo
}