const bcript = require('bcrypt-nodejs');
const Usuario = require('../modelo/usuario');
const jwt = require('../servicios/jwt');
const fs = require('fs');
const path = require('path');

// function pruebas(req, res){
//     res.status(200).send({
//         mensaje: "Probando controlador."
//     });
// }

function guardarUsuario(req, res) {
    var usuario = new Usuario;
    var parametros = req.body;

    console.log(parametros);

    usuario.nombre = parametros.nombre;
    usuario.apellido = parametros.apellido;
    usuario.correo = parametros.correo;
    usuario.rol = "usuario";
    usuario.imagen = null;

    if (parametros.contrasena) {
        bcript.hash(parametros.contrasena, null, null, function(err, hash) {
            usuario.contrasena = hash;
            if (usuario.nombre != null && usuario.apellido != null && usuario.correo != null) {
                usuario.save((err, usuarioStored) => {
                    if (err) {
                        res.status(500).send({ message: "Error en el servidor" });
                    } else {
                        if (!usuarioStored) {
                            res.status(404).send({ message: "No se registro" });
                        } else {
                            res.status(200).send({ usuario: usuarioStored });
                        }
                    }
                });
            } else {
                res.status(200).send({ message: "Faltan datos!" });
            }
        });
    } else {
        res.status(500).send({ message: "Introduce la contraseña" });
    }
}

function login(req, res) {
    var paramentros = req.body;
    var correoUsuario = paramentros.correo;
    var contraUsuario = paramentros.contrasena;

    Usuario.findOne({ correo: correoUsuario.toLowerCase() }, (err, usuario) => {
        if (err) {
            res.status(500).send({ message: "Error en la peticion" });
        } else {
            if (!usuario) {
                res.status(404).send({ message: "No existe señor." });
            } else {
                bcript.compare(contraUsuario, usuario.contrasena, function(err, check) {
                    if (check) {
                        if (paramentros.getHash) {
                            res.status(200).send({
                                token: jwt.createToken(usuario)
                            });
                        } else {
                            res.status(200).send({ usuario });
                        }
                    } else {
                        res.status(404).send({ message: "Su contraseña es erronea" });
                    }
                });
            }
        }
    })
}

function editarUsuario(req, res) {
    var usuarioId = req.params.id;
    // var editar = req.body;

    Usuario.findByIdAndUpdate(usuarioId, req.body, (err, userUpdated) => {
        if (err) {
            res.status(500).send({ message: "Error al actualizar" });
        } else {
            if (!userUpdated) {
                res.status(404).send({ message: "No se pudo actualizar" });
            } else {
                res.status(200).send({ user: userUpdated });
            }
        }
    });

    // Usuario.findByIdAndUpdate({_id: usuarioId}, editar).then(()=>{
    //     Usuario.findOne({_id: usuarioId}).then((usuario)=>{
    //         res.send(usuario)
    //     })
    // }).catch(next)
}

function subirImagen(req, res) {
    var usuarioId = req.params.id;
    var nombreArchivo = "No ha subido nada";

    if (req.files) {
        var rutaArchivo = req.files.imagen.path;
        var partirArchivo = rutaArchivo.split('\\');
        var nombreArchivo = partirArchivo[2];

        var extenImg = nombreArchivo.split('\.');
        var extenArchivo = extenImg[1];

        if (extenArchivo == 'png' || extenArchivo == 'jpg') {
            Usuario.findByIdAndUpdate(usuarioId, { imagen: nombreArchivo }, (err, userUpdated) => {
                if (err) {
                    res.status(500).send({ message: "Error al actualizar" });
                } else {
                    if (!userUpdated) {
                        res.status(404).send({ message: "No se pudo actualizar" });
                    } else {
                        res.status(200).send({ user: userUpdated });
                    }
                }
            });
        } else {
            res.status(200).send({ message: 'No es una imagen!!' })
        }

        console.log(rutaArchivo);
    } else {
        res.status(200).send({ message: 'No ha subido ninguna imagen' })
    }
}

function obtenerArchivo(req, res) {
    var archivo = req.params.imageFile;
    var rutaArchivo = './archivos/usuarios/' + archivo;

    fs.exists(rutaArchivo, function(exists) {
        if (exists) {
            res.sendFile(path.resolve(rutaArchivo));
        } else {
            res.status(200).send({ message: 'No existe la mk imagen' })
        }
    });
}

module.exports = {
    // pruebas,
    guardarUsuario,
    login,
    editarUsuario,
    subirImagen,
    obtenerArchivo
}