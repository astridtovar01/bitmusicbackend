const express = require('express');
const bodyParser = require('body-parser');

const app = express();

const usuarioRutas = require('./rutas/usuarioRutas');
const artistaRutas = require('./rutas/artistaRutas');
const albumRutas = require('./rutas/albumRutas');
const cancionRutas = require('./rutas/cancionRutas');

app.use(bodyParser.json());

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS');
    res.header('Allow', 'GET, PUT, POST, DELETE, OPTIONS');

    next();

})

app.use('/api', usuarioRutas);
app.use('/api', artistaRutas);
app.use('/api', albumRutas);
app.use('/api', cancionRutas);

module.exports = app;

// Configurar las rutas de express para nuestra API