var jwt = require('jwt-simple');
var secret = "soy_secreta";

exports.aseguraAuten = function(req, res, next){
    if(!req.headers.authorization){
        return res.status(403).send({message: "No existe la cabecera"});
    }

    var token = req.headers.authorization.replace(/[""]+/g, '');

    try{
        var payLoad = jwt.decode(token, secret);
    }catch(ex){
        console.log(ex);
        return res.status(404).send({message: "Token no valido"});
    }

    req.usuario = payLoad;

    next();
};